<?php 
    // echo 'hello, there!'
    // $conn = mysqli_connect('localhost', 'root', '', 'product_management');

    // if(!$conn) {
    //     echo 'Connection error: ' . mysqli_connect_error();
    // } 

    include('db_connection.php');

    if(isset($_POST['mass-delete'])) {

        $checkbox = $_POST['checkbox'];

        for($i=0;$i<count($checkbox);$i++){
            $del_id = $checkbox[$i]; 
            $sql = "DELETE FROM product WHERE id='".$del_id."'";
            mysqli_query($conn, $sql);
        }

        if(mysqli_query($conn, $sql)) {
            header('Location: list_product.php');
        } else {
            echo 'query error: '. mysqli_error($conn);
        }
    }




    if(isset($_POST['add'])) {

        $sql = 'SELECT sku,name,price,productType,size,height,length,width,weight,id FROM product ORDER BY id';

        if(mysqli_query($conn, $sql)) {
            header('Location: addProduct.php');
        } else {
            echo 'query error: '. mysqli_error($conn);
        }
    }



    $sql = 'SELECT sku,name,price,productType,size,height,length,width,weight,id FROM product ORDER BY id';
    $result = mysqli_query($conn, $sql);

    $products = mysqli_fetch_all($result, MYSQLI_ASSOC);

    mysqli_free_result($result);

    mysqli_close($conn);


?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" type="text/css" href="styles.css">
</head>
<body>
    <div class="fixed-header">
        <h1><a href="#">Product List</a></h1>
        
        <div class="container">
            <input type="submit" value="ADD" name="add" form="submittion">
            <input type="submit" value="MASS DELETE" name="mass-delete" form="submittion" id="id_to_delete">
        </div>
        
    </div>



    
    <div class="grid-container">
        <form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post" id="submittion">
        <?php foreach ($products as $key => $product) { ?>
            <div class="grid-item">
                <input type="checkbox" class="delete-checkbox" name='checkbox[]' value=<?php echo $product['id']; ?>>
                <div><?php echo $product['sku']?></div>
                <div><?php echo $product['name']?></div>
                <div><?php echo $product['price']." $"?></div>
                <div><?php if ($product['productType'] == 'dvd') { ?>
                    <div><?php echo $product['size']." MB"?></div>
                <?php } elseif ($product['productType'] == 'furniture') { ?>
                    <div><?php echo "Dimension: ".$product['height']."*".$product['width']."*".$product['length']?></div>                
                <?php }  elseif ($product['productType'] == 'book') { ?>
                    <div><?php echo "Weight: ".$product['weight']." KG"?></div>                
                <?php } ?></div>              
            </div>
            <?php } ?>
        </form>
    </div>


</body>
</html>